package ch.bbw.pr.cluedo.model;

/**
 * A Person
 * @author Peter Rutschmann
 * @version 29.08.2022
 */
public class Person {
   private String name;
   private String formOfAdress;
   private String colour;
   private String sex;

   public Person(String name, String formOfAdress, String colour, String sex) {
      this.name = name;
      this.formOfAdress = formOfAdress;
      this.colour = colour;
      this.sex = sex;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getFormOfAdress() {
      return formOfAdress;
   }

   public void setFormOfAdress(String formOfAdress) {
      this.formOfAdress = formOfAdress;
   }

   public String getColour() {
      return colour;
   }

   public void setColour(String colour) {
      this.colour = colour;
   }

   public String getSex() {
      return sex;
   }

   public void setSex(String sex) {
      this.sex = sex;
   }

   @Override
   public String toString() {
      return "Person{" +
              "name='" + name + '\'' +
              ", formOfAdress='" + formOfAdress + '\'' +
              ", colour='" + colour + '\'' +
              ", sex='" + sex + '\'' +
              '}';
   }
}
