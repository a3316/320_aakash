package ch.bbw.pr.cluedo.model;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * PunchListService
 *
 * @author Aakash Sethi
 * @version 11.09.2022
 */
@Service
public class DataService {
   private List<Person> persons = List.of(
         new Person("Mustard", "Colonel", "yellow", "male"),
         new Person("Scarlett", "Miss", "red", "female"),
           new Person("Plum", "Professor", "violett", "male"),
           new Person("Green", "Reverend", "green", "male")
   );

   public List<Person> getPersons() {
      return persons;
   }
}
